
/*
    Welcome to your first dbt model!
    Did you know that you can also configure models directly within SQL files?
    This will override configurations stated in dbt_project.yml

    Try changing "table" to "view" below
	
	--{{ config(materialized='table') }}
	
	config(materialized='view'
	
		post_hook="UPDATE {{ this }} SET city='westwood' "
		
		
run -m test_p --vars "{'cid':1}"

*/


{{ config(
	alias='customer_v', 
	materialized='view'

) }}

with source_data as (

	SELECT customer_id, city, state_code
	FROM warehouse.customers
	where customer_id = {{ var("cid") }}

)

select *
from source_data

/*
    Uncomment the line below to remove records with null `id` values
*/

-- where id is not null
