
/*



    Try changing "table" to "view" below
	
	--{{ config(materialized='table') }}
	
	config(materialized='view'
	
		, post_hook="UPDATE {{ this }} SET city='westwood' "
		

			
		
*/


{{ config(
	alias='customer_temp', 
	materialized='table'
	, post_hook="drop table {{ this }}; "
) }}

with source_data as (

	SELECT customer_id, city, state_code
	FROM warehouse.customers


)

select *
from source_data

/*
    Uncomment the line below to remove records with null `id` values
*/

-- where id is not null
