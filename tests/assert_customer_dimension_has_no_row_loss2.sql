-- dim_customers must have the same number of rows as its staging counterpart
-- Therefore return records where this isn't true to make the test fail

with final_result as (

SELECT * FROM {{ ref('my_first_dbt_model') }}
WHERE 1 = 2

)


select * from final_result

